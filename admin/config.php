<?php
// HTTP
define('HTTP_SERVER', 'http://localhost/~satria/apleasure/admin/');
define('HTTP_CATALOG', 'http://localhost/~satria/apleasure/');

// HTTPS
define('HTTPS_SERVER', 'http://localhost/~satria/apleasure/admin/');
define('HTTPS_CATALOG', 'http://localhost/~satria/apleasure/');

// DIR
define('DIR_APPLICATION', '/home/satria/public_html/apleasure/admin/');
define('DIR_SYSTEM', '/home/satria/public_html/apleasure/system/');
define('DIR_LANGUAGE', '/home/satria/public_html/apleasure/admin/language/');
define('DIR_TEMPLATE', '/home/satria/public_html/apleasure/admin/view/template/');
define('DIR_CONFIG', '/home/satria/public_html/apleasure/system/config/');
define('DIR_IMAGE', '/home/satria/public_html/apleasure/image/');
define('DIR_CACHE', '/home/satria/public_html/apleasure/system/storage/cache/');
define('DIR_DOWNLOAD', '/home/satria/public_html/apleasure/system/storage/download/');
define('DIR_LOGS', '/home/satria/public_html/apleasure/system/storage/logs/');
define('DIR_MODIFICATION', '/home/satria/public_html/apleasure/system/storage/modification/');
define('DIR_UPLOAD', '/home/satria/public_html/apleasure/system/storage/upload/');
define('DIR_CATALOG', '/home/satria/public_html/apleasure/catalog/');

// DB
define('DB_DRIVER', 'mysqli');
define('DB_HOSTNAME', 'localhost');
define('DB_USERNAME', 'root');
define('DB_PASSWORD', '');
define('DB_DATABASE', 'apleasure');
define('DB_PORT', '3306');
define('DB_PREFIX', 'oc_');

<?php
class ControllerStaticAbout extends Controller {
	public function index() {
		$this->document->setTitle($this->config->get('config_meta_title'));
		$this->document->setDescription($this->config->get('config_meta_description'));
		$this->document->setKeywords($this->config->get('config_meta_keyword'));

		if (isset($this->request->get['route'])) {
			$this->document->addLink(HTTP_SERVER, 'canonical');
		}

		$data['footer'] = $this->load->controller('common/footer');
		$data['header'] = $this->load->controller('common/header');

		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/static/about.tpl')) {
			$this->response->setOutput($this->load->view($this->config->get('config_template') . '/template/static/about.tpl', $data));
		} else {
			$this->response->setOutput($this->load->view('default/template/static/about.tpl', $data));
		}
	}
}
